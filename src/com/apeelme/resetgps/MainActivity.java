package com.apeelme.resetgps;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
    
public class MainActivity extends Activity implements LocationListener, GpsStatus.Listener {
         
     private static String logtag = "resetgps";//for use as the tag when logging 
     private LocationManager mService;
     private LocationProvider mProvider;
     
     private static MainActivity sInstance;
     
     static MainActivity getInstance() {
         return sInstance;
     }
     
     private void sendExtraCommand(String command) {
         mService.sendExtraCommand(LocationManager.GPS_PROVIDER, command, null);
     }
       
     /** Called when the activity is first created. */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            sInstance = this;

            mService = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            mProvider = mService.getProvider(LocationManager.GPS_PROVIDER);
            if (mProvider == null) {
                // FIXME - fail gracefully here
                Log.e(logtag, "Unable to get GPS_PROVIDER");
            }
            mService.addGpsStatusListener(this);
            setContentView(R.layout.activity_main);
             
            Button buttonStart = (Button)findViewById(R.id.button1);        
         buttonStart.setOnClickListener(startListener); // Register the onClick listener with the implementation above
          

        }
         
        //Create an anonymous implementation of OnClickListener
        private OnClickListener startListener = new OnClickListener() {
            public void onClick(View v) {
              //Log.d(logtag,"onClick() called - start button");              
              Toast.makeText(MainActivity.this, "GPS reset folyamatban...", Toast.LENGTH_LONG).show();
              sendExtraCommand("delete_aiding_data");
              //Log.d(logtag,"onClick() ended - start button");
              Toast.makeText(MainActivity.this, "Kész!", Toast.LENGTH_LONG).show();
            }
        };
         
             
        @Override
     protected void onStart() {//activity is started and visible to the user
      Log.d(logtag,"onStart() called");
      super.onStart();  
     }
     @Override
     protected void onResume() {//activity was resumed and is visible again
      Log.d(logtag,"onResume() called");
      super.onResume();
       
     }
     @Override
     protected void onPause() { //device goes to sleep or another activity appears
      Log.d(logtag,"onPause() called");//another activity is currently running (or user has pressed Home)
      super.onPause();
       
     }
     @Override
     protected void onStop() { //the activity is not visible anymore
      Log.d(logtag,"onStop() called");
      super.onStop();
       
     }
     @Override
     protected void onDestroy() {//android has killed this activity
       Log.d(logtag,"onDestroy() called");
       super.onDestroy();
     }
    
    
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	public void onGpsStatusChanged(int event) {
		// TODO Auto-generated method stub
		
	}

	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
}
